﻿//using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using SeleniumBDD.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SeleniumBDD.PageObjects
{
    public class ProviderPage
    {
        public IWebDriver WebDriver;
        Actions myAction;
        public String OrgName = "NDIS" + GeneralUtility.randumNumber();
        public ProviderPage(IWebDriver WebDriver)
        {
            this.WebDriver = WebDriver;
            myAction = new Actions(WebDriver);
            PageFactory.InitElements(WebDriver, this);
            //WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(15));
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
        }
        [FindsBy(How = How.XPath, Using = "//a[contains(@href,'searchorganisation')]")] public IWebElement ProviderSearch { get; set; }
        [FindsBy(How = How.Id, Using = "txtBusinessName")] public IWebElement GiveProviderName { get; set; }
        [FindsBy(How = How.Id, Using = "btnSearch")] public IWebElement SubmitSearch { get; set; }
        [FindsBy(How = How.Id, Using = "btnRegNewOrganization")] public IWebElement NewProviderRegister { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'Organization.name')]")] public IWebElement TradingName { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'legalEntityName')]")] public IWebElement LegalEntityName { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'ABN')]")] public IWebElement ABN { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'Organization.address.line')]")] public IWebElement HeadOfficeAddress { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'Organization.address.postalCode')]")] public IWebElement PostCode { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'Organization.telecom:website.value')]")] public IWebElement WebSite { get; set; }
        [FindsBy(How = How.Id, Using = "btnContinue")] public IWebElement BtnContinue { get; set; }
        [FindsBy(How = How.Id, Using = "btnSetupProfile")] public IWebElement SetUpProfile { get; set; }

       [FindsBy(How = How.XPath, Using = "//textarea[contains(@data-fhirq-question-link-id,'Organization.extension:comment.valueString')]")] public IWebElement description { get; set; }
        [FindsBy(How = How.Id, Using = "btnSave")] public IWebElement BtnSave { get; set; }
        [FindsBy(How = How.XPath, Using = "//a[contains(@href,'ome')]")] public IWebElement Home { get; set; }
        [FindsBy(How = How.XPath, Using = "//a[contains(@href,'searchorganisation')]")] public IWebElement Provider { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'Location.name')]")] public IWebElement LocationName { get; set; }
        [FindsBy(How = How.XPath, Using = "//textarea[contains(@data-fhirq-question-link-id,'Location.description')]")] public IWebElement LocationDescription { get; set; }
        [FindsBy(How = How.Id, Using = "txtPhoneNo")] public IWebElement PhoneNumber { get; set; }
        [FindsBy(How = How.Id, Using = "txtEmail")] public IWebElement EmailAddress { get; set; }
        [FindsBy(How = How.Id, Using = "btnSupportCategoryContinue")] public IWebElement CategoryContinue { get; set; }
        [FindsBy(How = How.Id, Using = "btnAddService")] public IWebElement AddServiceButton { get; set; }
        [FindsBy(How = How.Id, Using = "btnSubmit")] public IWebElement SubmitButton { get; set; }
        [FindsBy(How = How.Id, Using = "btnStaff")] public IWebElement AddStaffButton { get; set; }
        [FindsBy(How = How.Id, Using = "btnDoThisLater")] public IWebElement DoThisLaterButton { get; set; }
        [FindsBy(How = How.Id, Using = "btnClaim")] public IWebElement ClaimButton { get; set; }
        [FindsBy(How = How.Id, Using = "txtJobTitle")] public IWebElement JobTitle { get; set; }
        [FindsBy(How = How.Id, Using = "chkBehalfOrganisation")] public IWebElement IdeclareChkBox { get; set; }
        [FindsBy(How = How.Id, Using = "chkSecurityCodeNotReceived")] public IWebElement SecurityCodeNotReceived { get; set; }
        [FindsBy(How = How.Id, Using = "txtSecurityCode")] public IWebElement EnterSecurityCode { get; set; }
        //*****************AddStaff**********************
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'Practitioner.name.given')]")] public IWebElement WorkerFirstName { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'Practitioner.name.family')]")] public IWebElement WorkerLastName { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'Practitioner.practitionerRole.role.text')]")] public IWebElement WorkerRole { get; set; }
       
        [FindsBy(How = How.XPath, Using = "//textarea[contains(@data-fhirq-question-link-id,'Practitioner.extension:bio.valueString')]")] public IWebElement WorkerBio { get; set; }
        [FindsBy(How = How.Id, Using = "btnChooseServices")] public IWebElement ChooseServicesButton { get; set; }

        //*****************AddStaff**********************

        public void addWorkers()
        {
            AddStaffButton.Click();
            WorkerFirstName.SendKeys("John");
            WorkerLastName.SendKeys("Gillies");
            WorkerRole.SendKeys("manager");
            WorkerBio.SendKeys("graduate");
            //add photo
            WebDriver.FindElements(By.CssSelector(".button-inside-label")).ElementAt(0).Click();
            System.Diagnostics.Process.Start(@"D:\WorkSpace\CSharp\Bharath\SeleniumBDD\SeleniumBDD\DataFiles\UploadLogo.exe");
            WebDriver.FindElements(By.XPath("//input[contains(@data-fhirq-question-link-id,'Practitioner.gender')]")).ElementAt(1).Click();        
            //add locations
            WebDriver.FindElements(By.ClassName("checkbox")).ElementAt(0).Click();
            //choose services
            ChooseServicesButton.Click();
            if(WebDriver.FindElements(By.Name("chkService")).Count > 0)
                WebDriver.FindElements(By.Name("chkService")).ElementAt(0).Click();
            WebDriver.FindElement(By.Id("btnSaveService")).Click();
            BtnSave.Click();
            WebDriver.FindElement(By.Id("btnFinish")).Click();

        }
        public Boolean confirmProfileFinished()
        {
           return  WebDriver.FindElements(By.ClassName("main-content-text")).ElementAt(7).GetAttribute("innerText").Contains("You're done");
        }
        public void SearchOrganizationByName()
        {
           // WebDriver.Navigate().GoToUrl(ConfigurationManager.AppSettings["appUrl"]);
            ProviderSearch.Click();
            GiveProviderName.SendKeys(OrgName);
            SubmitSearch.Click();
        }
        public void SearchOrganizationByRandomName()
        {
            // WebDriver.Navigate().GoToUrl(ConfigurationManager.AppSettings["appUrl"]);
            ProviderSearch.Click();
            GiveProviderName.SendKeys("fdfdfdd");
            SubmitSearch.Click();
        }
        public Boolean OrganizationAvailableInDataBase()
        {
                return NewProviderRegister.Enabled;
        }
        public Boolean RegisterNewOrganizationOption()
        {
            return NewProviderRegister.Enabled;
        }
        public void SelectRegisterOrganization()
        {
            Thread.Sleep(8000);
            WebDriver.FindElement(By.Id("btnRegNewOrganization")).SendKeys(Keys.Enter);
            WebDriver.FindElement(By.Id("btnRegiser")).SendKeys(Keys.Enter);
            //NewProviderRegister.Click();
        }
        public Boolean ConfirmRegistrationPage()
        {
            return WebDriver.FindElement(By.Id("btnRegister")).Enabled;
        }

        public void FillRegistrationForm()
        {
            WebDriver.FindElement(By.Id("txtFirstName")).SendKeys("Bharath");
            WebDriver.FindElement(By.Id("txtLastName")).SendKeys("Gaddam" );
            WebDriver.FindElement(By.Id("txtSuburb")).SendKeys("PARRAMATTA, 2123 NSW");
            Thread.Sleep(2000);
           // myAction.SendKeys(Keys.ArrowDown).Build().Perform();
           // myAction.SendKeys(Keys.Enter).Build().Perform();
           // myAction.SendKeys(Keys.Tab).Build().Perform();
           // WebDriver.FindElement(By.Id("txtSuburb")).SendKeys("PARRAMATTA, 2123 NSW");
            WebDriver.FindElement(By.Id("txtPhoneNo")).SendKeys("612368"+ GeneralUtility.randumNumber());
           // WebDriver.FindElement(By.Id("txtEmail")).SendKeys(ConfigurationManager.AppSettings["GmailUserName"]  + "@gmail.com");
            WebDriver.FindElement(By.Id("txtEmail")).SendKeys(providerEmailAddress);
            WebDriver.FindElement(By.Id("btnRegister")).SendKeys(Keys.Enter);
        }
        public static String providerEmailAddress = ConfigurationManager.AppSettings["GmailUserName"] + "+" + GeneralUtility.randumNumber() + "@gmail.com" ;
        public void verifyUserRegistration()
        {
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.ElementAt(1));
            WebDriver.FindElement(By.Id("txtPassword")).SendKeys("Ndis@1234");
            WebDriver.FindElement(By.Id("txtConfirmPassword")).SendKeys("Ndis@1234");
            WebDriver.FindElement(By.Id("btnRegister")).Click();
        }

        public void continuLoginToNDIS()
        {
            WebDriver.FindElement(By.XPath("//a[contains(@href,'signin')]")).Click();
        }

        public void AddYourOrganization()
        {
            //ProviderSearch.Click();
            //GiveProviderName.SendKeys(OrgName);
            //SubmitSearch.Click();
            TradingName.SendKeys(OrgName);
            LegalEntityName.SendKeys("orgLegalEntity");
            ABN.SendKeys("12345678901");
            HeadOfficeAddress.SendKeys("Org Address");
            PostCode.SendKeys("MELBERGEN, 2669 NSW");
            Thread.Sleep(1000);
            // myAction.KeyDown(Keys.Down).SendKeys(Keys.Enter).Build().Perform();
            WebSite.SendKeys("www.google.com");
            PhoneNumber.SendKeys("6123456787");
            EmailAddress.SendKeys(ConfigurationManager.AppSettings["GmailUserName"]+"@gmail.com");
            BtnContinue.Click();
        }

        public Boolean ConfirmRegistration()
        {
            return SetUpProfile.Enabled;
        }
        public Boolean SetUpProviderProfile()
        {
            return SetUpProfile.Enabled;
        }
        public void addBranding()
        {
            //WebDriver.FindElement(By.Id("btnSetupProfile")).SendKeys(Keys.Enter);
            Thread.Sleep(8000);
            //SetUpProfile
            WebDriver.FindElements(By.ClassName("provider-button-dark-large")).ElementAt(4).Click();
            //Add Logo
            Thread.Sleep(8000);
            WebDriver.FindElements(By.CssSelector(".button-inside-label")).ElementAt(0).Click();
            System.Diagnostics.Process.Start(@"D:\WorkSpace\CSharp\Bharath\SeleniumBDD\SeleniumBDD\DataFiles\UploadLogo.exe");
            //add description
            description.SendKeys("NDIS provider");
            WebDriver.FindElements(By.XPath("//input[contains(@name,'Organization.extension:ndis-reg-status.valueCoding')]")).ElementAt(0).Click();
            //Add Banner
            Thread.Sleep(8000);
            WebDriver.FindElements(By.CssSelector(".button-inside-label")).ElementAt(1).Click();
            System.Diagnostics.Process.Start(@"D:\WorkSpace\CSharp\Bharath\SeleniumBDD\SeleniumBDD\DataFiles\UploadLogo.exe");
            BtnSave.Click();
        }

        public void addLocations()
        {
            Thread.Sleep(10000);
            //WebDriver.FindElement(By.Id("btnSetupProfile")).SendKeys(Keys.Enter);
            WebDriver.FindElements(By.ClassName("provider-button-dark-large")).ElementAt(0).Click();
            LocationName.SendKeys("123 Main");
            LocationDescription.SendKeys("123 Main Street");
            PhoneNumber.SendKeys("6123457809");
            EmailAddress.SendKeys("ndis@gmail.com");
            selectRegion("Northern Sydney");
            selectRegion("Katherine region");
            selectRegion("Brisbane");
            selectRegion("Southern Adelaide");
            selectRegion("Western Melbourne");
            selectFecility("Wheelchair access");
            selectFecility("Baby Change Facility");
            selectFecility("TTY facilities");
            BtnSave.Click();
        }

        public void EditLocations()
        {
            Thread.Sleep(10000);
           // WebDriver.FindElement(By.Id("allOfAustralia")).Click();
           // WebDriver.FindElement(By.Id("allOfAustralia")).Click();
            selectLocation("Central Coast");
            selectLocation("Barkly");
            selectLocation("Bundaberg");
            selectLocation("Limestone Coast");
            EditFecilities();
            BtnSave.Click();
        }

        public void EditFecilities()
        {
            editFecility("Accessible Telephones");
            editFecility("Disabled Parking");
            editFecility("Toilet");
        }
        public void clickEditLocations()
        {
            WebDriver.FindElement(By.Id("btnEditLocation")).Click();
        }
        public void addServices()
        {
            AddServiceButton.Click();
            selectService("Assistance With Daily Living");
            selectService("Home Modifications");
            selectService("Improved Living Arrangements");
            selectService("Improved Relationships");
            selectService("Improved Life Choices");
            CategoryContinue.Click();
        }
        public void addServices_edit()
        {
            NavigateToMyProfileNavLinks("Services");
            AddServiceButton.Click();
            selectSupportCategory("Assistance With Daily Living");
            selectSupportCategory("Transport");
            selectSupportCategory("Consumables");
            CategoryContinue.Click();
            //Assistance With Daily Living
            ChooseServices("Accommodation/Tenancy", 0);
            ChooseServices("Daily Personal Activities", 6);
            SubmitButton.Click();
            //Transport
            ChooseServices("Assistance with Travel and/or Transport", 0);
            SubmitButton.Click();
            //Consumables
            ChooseServices("Assistive products for personal care and safety", 0);
            SubmitButton.Click();

        }

        public void ChooseServices(String supportCategory,int number)
        {
            chooseService(supportCategory);
            ndisRegisteredStatus(number);
        }
        public void selectAssitanceWithDailyLiving()
        {
            //assitanceWithDailyLiving();
            assitanceWithDailyLiving("Accommodation/Tenancy");
            assitanceWithDailyLiving("Yes");
            assitanceWithDailyLiving("123 Main");

            assitanceWithDailyLiving("Assistance with Personal Activities");
            assitanceWithDailyLiving("Yes");
            assitanceWithDailyLiving("123 Main");

            assitanceWithDailyLiving("Assistance with Personal Activities (high intensity)");
            assitanceWithDailyLiving("No");
            assitanceWithDailyLiving("123 Main");


            SubmitButton.Click();
            //DoThisLaterButton.Click();
        }
        public void selectLocation(String id)
        {
            WebDriver.FindElement(By.Id(id)).Click();
        }
        public void editFecility(String id)
        {
            WebDriver.FindElement(By.Id(id)).Click();
        }
        public void selectRegion(String regionName)
        {
         ReadOnlyCollection<IWebElement> allRegions = WebDriver.FindElements(By.ClassName("checkbox"));
            for(int i=0;i< allRegions.Count;i++)
            {
               if(allRegions.ElementAt(i).GetAttribute("innerText").Contains(regionName))
                {
                    allRegions.ElementAt(i).Click();
                    break;
                }
            }
        }

        public void selectFecility(String fecilityName)
        {
            ReadOnlyCollection<IWebElement> allRegions = WebDriver.FindElements(By.ClassName("checkbox"));
            for (int i = 0; i < allRegions.Count; i++)
            {
                if (allRegions.ElementAt(i).GetAttribute("innerText").Contains(fecilityName))
                {
                    allRegions.ElementAt(i).Click();
                    break;
                }
            }
        }
        public void selectSupportCategory(String supportCategory)
        {
            ReadOnlyCollection<IWebElement> allServices = WebDriver.FindElements(By.ClassName("support-category-title"));
            for (int i = 0; i < allServices.Count; i++)
            {
                if (allServices.ElementAt(i).GetAttribute("innerText").Contains(supportCategory))
                {
                    allServices.ElementAt(i).Click();
                    break;
                }
            }
        }
        public void selectService(String serviceName)
        {
            ReadOnlyCollection<IWebElement> allServices = WebDriver.FindElements(By.ClassName("support-category-title"));
            for (int i = 0; i < allServices.Count; i++)
            {
                if (allServices.ElementAt(i).GetAttribute("innerText").Contains(serviceName))
                {
                    allServices.ElementAt(i).Click();
                    break;
                }
            }
        }
        public void assitanceWithDailyLiving()
        {
            ReadOnlyCollection<IWebElement> allAssistance = WebDriver.FindElements(By.ClassName("rg-service-display"));
            for (int i = 0; i < allAssistance.Count; i++)
            {
                
                    allAssistance.ElementAt(i).Click();
                
            }
        }
        public void assitanceWithDailyLiving(String assitanceName)
        {
            //ReadOnlyCollection<IWebElement> allAssistance = WebDriver.FindElements(By.ClassName("rg-service-display"));
            ReadOnlyCollection<IWebElement> allAssistance = WebDriver.FindElements(By.ClassName("pt15px"));
            for (int i = 0; i < allAssistance.Count; i++)
            {
                if (allAssistance.ElementAt(i).GetAttribute("innerText").Contains(assitanceName))
                {
                    allAssistance.ElementAt(i).Click();
                    break;
                }
            }
        }

        public void chooseService(String serviceName)
        {
            //ReadOnlyCollection<IWebElement> allAssistance = WebDriver.FindElements(By.ClassName("rg-service-display"));
            ReadOnlyCollection<IWebElement> allAssistance = WebDriver.FindElements(By.ClassName("pt15px"));
            for (int i = 0; i < allAssistance.Count; i++)
            {
                if (allAssistance.ElementAt(i).GetAttribute("innerText").Contains(serviceName))
                {
                    allAssistance.ElementAt(i).Click();
                    break;
                }
            }
        }
        public void ndisRegisteredStatus(int number)
        {
            //ReadOnlyCollection<IWebElement> allAssistance = WebDriver.FindElements(By.ClassName("rg-service-display"));
            ReadOnlyCollection<IWebElement> allAssistance = WebDriver.FindElements(By.ClassName("rg-service-display"));
            allAssistance.ElementAt(number).Click();
            allAssistance.ElementAt(number+2).Click();
            allAssistance.ElementAt(number + 3).Click();
            allAssistance.ElementAt(number + 4).Click();
            allAssistance.ElementAt(number + 5).Click();
        }
        public Boolean confirmProfilePage()
        {
            return WebDriver.FindElement(By.XPath("//a[contains(@href,'myprofile')]")).Displayed;
        }
        public Boolean confirmLocationDetailsPage()
        {
            return WebDriver.Url.Contains("location");
        }
        public void selectViewFromMyProfile()
        {
            WebDriver.FindElement(By.XPath("//a[contains(@href,'myprofile')]")).Click();
            Thread.Sleep(5000);
            WebDriver.FindElements(By.ClassName("tile-action-button")).ElementAt(0).Click();

        }
        public void NavigateToMyProfileNavLinks(String profileTab)
        {
            Thread.Sleep(10000);
            IList<IWebElement> collection = WebDriver.FindElements(By.ClassName("nav-link"));
            for(int i=0;i< collection.Count; i++)
            {
                if(profileTab.Contains(collection.ElementAt(i).Text))
                {
                    collection.ElementAt(i).Click();
                    break;
                }
            }
        }

        public bool confirmOrganizationName()
        {
            return WebDriver.FindElement(By.ClassName("page-header1")).GetAttribute("innerText").Trim().Equals(OrgName);
        }

        public async Task<string> getSecurityCodeAsync()
        {
            
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync("http://ndis-api-tst.npd.telstrahealth.com/api/v1/userorg/get/securitycode/"+OrgName);
            if (response.IsSuccessStatusCode)
            {
                var jsonData = await response.Content.ReadAsStringAsync();
                dynamic data = JObject.Parse(jsonData);
                securityCode = data.DataObject[0].SecurityCode.Value;
            }
            return securityCode.ToString();
        }
        String securityCode;
        public void claimTheOrganization()
        {
            ClaimButton.Click();
            JobTitle.SendKeys("CEO");
            Thread.Sleep(2000);
            IdeclareChkBox.Click();
            Thread.Sleep(2000);
            SecurityCodeNotReceived.Click();
            BtnSave.Click();
        }
        public void claimTheOrganizationWithSecurityCode()
        {
            ClaimButton.Click();
            JobTitle.Clear();
            JobTitle.SendKeys("CEO");
            Thread.Sleep(4000);
            IdeclareChkBox.Click();
            Thread.Sleep(2000);
            EnterSecurityCode.SendKeys(securityCode);
            BtnSave.Click();
        }
    }
}
