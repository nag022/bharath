﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace SeleniumBDD.PageObjects
{

    public class AnanymousUserPage
    {
        public IWebDriver WebDriver;
        public AnanymousUserPage(IWebDriver WebDriver)
        {
            this.WebDriver = WebDriver;
            PageFactory.InitElements(WebDriver, this);
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
        }
       
        [FindsBy(How = How.Id, Using = "consumer-sign-up-button")] private IWebElement RegisterObj;
        [FindsBy(How = How.Id, Using = "txtFirstName")] private IWebElement firstNameObj;
        [FindsBy(How = How.Id, Using = "txtLastName")] private IWebElement lastNameObj;
        [FindsBy(How = How.Id, Using = "txtSuburb")] private IWebElement subUrbObj;
        [FindsBy(How = How.Id, Using = "txtPhoneNo")] private IWebElement phNumberObj;
        [FindsBy(How = How.Id, Using = "txtEmail")] private IWebElement emailObj;
        [FindsBy(How = How.Id, Using = "btnRegister")] private IWebElement submitRegisterObj;

        public Boolean ConfirmNDIShomePage()
        {
            return RegisterObj.Enabled;
        }
        public void CallRegister()
        {
            RegisterObj.Click();
        }

        public Boolean confirmRegistrationForm()
        {
            return firstNameObj.Enabled;
        }
        public void fillRegistrationForm()
        {
            firstNameObj.SendKeys("Steve");
            lastNameObj.SendKeys("Johnson");
            subUrbObj.SendKeys("EI");
            phNumberObj.SendKeys("123456789");
            emailObj.SendKeys("steve@gmail.com");
            submitRegisterObj.Click();

        }
        public Boolean ConfirmThankYouMessage()
        {
           String successMsg = WebDriver.FindElements(By.ClassName("main-content-text")).ElementAt(0).GetAttribute("innerText");
           return(successMsg.Contains("Thank you for registering with My Life, My Choices"));
        }

    }
}
