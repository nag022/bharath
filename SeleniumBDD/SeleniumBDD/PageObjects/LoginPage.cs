﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeleniumBDD.PageObjects
{
    public class LoginPage
    {
        public IWebDriver WebDriver;
        Actions myActions;
        //login page costructorD:\WorkSpace\CSharp\Bharath\SeleniumBDD\SeleniumBDD\PageObjects\LoginPage.cs
        public LoginPage(IWebDriver WebDriver)
        {
            this.WebDriver = WebDriver;
            myActions = new Actions(WebDriver);
            PageFactory.InitElements(WebDriver, this);
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
        }
        [FindsBy(How = How.XPath, Using = "//a[contains(@href,'signin')]")] private IWebElement signInButton;
        [FindsBy(How = How.Id, Using = "txtEmail")] private IWebElement emailAddress;
        [FindsBy(How = How.Id, Using = "txtPassword")] private IWebElement password;
        [FindsBy(How = How.Id, Using = "btnSignIn")] private IWebElement loginButton;
        public void launchApplication()
        {
            WebDriver.Navigate().GoToUrl(ConfigurationManager.AppSettings["appUrl"]);
        }

        public void callSignPage()
        {
            signInButton.Click();
        }

        public void loginToApplication()
        {
           
           // emailAddress.SendKeys(ProviderPage.providerEmailAddress);
            emailAddress.SendKeys(ConfigurationManager.AppSettings["username"]);
            password.SendKeys(ConfigurationManager.AppSettings["password"]);
            loginButton.Click();

        }
        public void loginToApplication(string userName,String passWord)
        {

            emailAddress.SendKeys(userName);
            password.SendKeys(passWord);
            loginButton.Click();

        }

        public void logoutApplication()
        {
            WebDriver.Navigate().GoToUrl("https://ndis-web-tst.azurewebsites.net/signin");
            ////*[@id="mainNav"]/div/div[2]/ul/li[5]/a
           // myActions.MoveToElement(WebDriver.FindElement(By.XPath("//*[@id='mainNav']/div/div[2]/ul/li[5]/a"))).Click().Build().Perform();
           // WebDriver.FindElement(By.XPath("//*[@id='mainNav']/div/div[2]/ul/li[5]/a")).Click();
           // WebDriver.FindElement(By.XPath("//*[@id='mainNav']/div/div[2]/ul/li[5]/a")).SendKeys(Keys.Enter);
          //  IReadOnlyCollection<IWebElement> collection  = WebDriver.FindElements(By.CssSelector(".page-scroll"));
          //for(int i=0;i<collection.Count;i++)
          //  {
          //      if(collection.ElementAt(i).GetAttribute("innerText").Contains("Sign Out"))
          //      {
                   
          //          collection.ElementAt(i).FindElement(By.TagName("a")).Click();
                    
          //          break;
          //      }
          //  }
        }

        public void confirmUserLogin()
        {
           // WebDriver.Navigate().GoToUrl(ConfigurationManager.AppSettings["appUrl"]);
        }
    }
}
