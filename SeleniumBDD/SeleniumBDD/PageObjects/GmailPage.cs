﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using SeleniumBDD.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeleniumBDD.PageObjects
{
    public class GmailPage
    {
        public IWebDriver webDriver;
        public Actions myAction;
        TableElement _webTable;
        int iteration;
        String emailSubject1 = "Registration Confirmation You have successfully completed your registration";
        String emailSubject2 = "Your NDIS security code";
        public GmailPage(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
            myAction = new Actions(webDriver);
            PageFactory.InitElements(webDriver, this);
            //WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(15));
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            //myInbox = new TableElement(webDriver.FindElement(By.XPath("//table[contains(@class,'zt')]")));
        }

        [FindsBy(How = How.Id, Using = "identifierId")] public IWebElement gmailUserName { get; set; }
        [FindsBy(How = How.Name, Using = "password")] public IWebElement gmailPassWord { get; set; }
        [FindsBy(How = How.ClassName, Using = "CwaK9")] public IWebElement gmailNext { get; set; }

        [FindsBy(How = How.XPath, Using = "//table[contains(@class,'zt')]")] public IWebElement myInbox { get; set; }

        public void launchGmail()
        {
            webDriver.Manage().Cookies.DeleteAllCookies();
            webDriver.Navigate().GoToUrl("https://gmail.com");
            
        }
        
        public void GmailLogin(int iteration)
        {
            if (iteration < 1)
            {
                gmailUserName.SendKeys(ConfigurationManager.AppSettings["GmailUserName"] + "@gmail.com");
                gmailNext.Click();
            }
            Thread.Sleep(2000);
            gmailPassWord.SendKeys(ConfigurationManager.AppSettings["GmailPassWord"]);
            gmailNext.Click();
        }
       
        public void DeleteRegistrationEmail()
        {
            
            bool flag = false;
            IList<IWebElement> allRows = myInbox.FindElements(By.TagName("tr"));
            for(int i=0;i<allRows.Count;i++)
            {
                
                for (int j=0; j< allRows.ElementAt(i).FindElements(By.TagName("td")).Count;j++)

                {
                    Debug.WriteLine(allRows.ElementAt(i).FindElements(By.TagName("td")).ElementAt(j).Text);
                    string actualText = allRows.ElementAt(i).FindElements(By.TagName("td")).ElementAt(j).Text;
                    if (actualText.Contains(emailSubject1) || actualText.Contains(emailSubject2))
                    {
                        allRows.ElementAt(i).FindElements(By.TagName("td")).ElementAt(1).Click();
                        flag = true;
                        break;
                    }
                }
               //if(flag)
               // {
               //     break;
               // }
            }
            if (flag)
            {
                webDriver.FindElement(By.CssSelector(".ar9")).Click();
            }
            
        }
        public void OpenRegistrationEmail()
        {
            bool flag = false;
            IList<IWebElement> allRows = myInbox.FindElements(By.TagName("tr"));
            for (int i = 0; i < allRows.Count; i++)
            {
                for (int j = 0; j < allRows.ElementAt(i).FindElements(By.TagName("td")).Count; j++)
                {
                    string actualText = allRows.ElementAt(i).FindElements(By.TagName("td")).ElementAt(j).Text;
                    if (actualText.Contains(emailSubject1) || actualText.Contains(emailSubject2))
                    {
                        allRows.ElementAt(i).FindElements(By.TagName("td")).ElementAt(4).Click();
                        flag = true;
                        break;
                    }
                }
                if (flag)
                {
                    break;
                }
            }
        }

        public void BackToMyChooser()
        {
            webDriver.FindElement(By.XPath("//a[contains(@data-saferedirecturl,'telstrahealth.com/registration/verify')]")).Click();
        }

        public void gmailLogout()
        {
            try
            {
                webDriver.FindElement(By.CssSelector(".gb_bb")).Click();
                webDriver.FindElement(By.XPath("//a[contains(@href,'accounts.google.com/Logout')]")).Click();
                webDriver.SwitchTo().Alert().Accept();
            }
            catch(Exception e)
            {
                Debug.WriteLine("Continue if not confirmation asked");
            }
            
        }
    }
}
