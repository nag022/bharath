﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using SeleniumBDD.PageObjects;
using SeleniumBDD.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using TechTalk.SpecFlow;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Threading;

namespace SeleniumBDD.StepDefinitions
{
    [Binding]
    public class ProviderSteps
    {
        public static IWebDriver WebDriver;
        public LoginPage _loginPage;
        public ProviderPage _providerPage;
        public GmailPage _gmailPage;
        public ProviderSteps()
        {
            WebDriver = TestSetup.WebDriver;
            _loginPage = new LoginPage(WebDriver);
            _providerPage = new ProviderPage(WebDriver);
            _gmailPage = new GmailPage(WebDriver);

        }
        [Given(@"As Authorised provider I call NDIS home page")]
        public void GivenAsAuthorisedProviderICallNDISHomePage()
        {
            _loginPage.launchApplication();
        }

        [When(@"I Search for an organization by name")]
        public void WhenISearchForAnOrganizationByName()
        {
            _providerPage.SearchOrganizationByName();
        }

        [When(@"the searched organization is not listed in the NDIS database")]
        public void WhenTheSearchedOrganizationIsNotListedInTheNDISDatabase()
        {
            _providerPage.OrganizationAvailableInDataBase();
        }

        [Then(@"I should be suggested to Register a new organization")]
        public void ThenIShouldBeSuggestedToRegisterANewOrganization()
        {
           // _providerPage.RegisterNewOrganizationOption();
            if (_providerPage.RegisterNewOrganizationOption())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "User can see the Register NewOrganization Option", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "User can not see the Register NewOrganization Option ", null);
            }
        }

        [When(@"I clicks on Register a new organization button")]
        public void WhenIClicksOnRegisterANewOrganizationButton()
        {
            _providerPage.SelectRegisterOrganization();
        }

        [Then(@"I Should see the Registration Page")]
        public void ThenIShouldSeeTheRegistrationPage()
        {
            //_providerPage.ConfirmRegistrationPage();
            if (_providerPage.ConfirmRegistrationPage())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "User can see the ConfirmRegistrationPage", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "User can not see the ConfirmRegistrationPage ", null);
            }
        }

        [When(@"I fill all the Details and Submit the registration page")]
        public void WhenIFillAllTheDetailsAndSubmitTheRegistrationPage()
        {
            _providerPage.FillRegistrationForm();
        }

        [Then(@"I should be registered successfully")]
        public void ThenIShouldBeRegisteredSuccessfully()
        {
            //Assert.IsTrue(_providerPage.ConfirmRegistration()); 
            //if (_providerPage.ConfirmRegistration())
            if(WebDriver.FindElements(By.XPath("//h1[contains(@class,'col-md-12')]")).ElementAt(0).GetAttribute("innerText").Contains("Thank you for registering with MyChoozer"))
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "User Registration Confirmed", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "User Registration Failed ", null);
            }
        }

        [When(@"I Login To Gmail and Select BackToMyChooser")]
        public void WhenILoginToGmailAndSelectBackToMyChooser()
        {
            _gmailPage.launchGmail();
            _gmailPage.GmailLogin(1);
            _gmailPage.OpenRegistrationEmail();
            _gmailPage.BackToMyChooser();
        }

        [Then(@"I Could able to finish the provider registration process")]
        public void ThenICouldAbleToFinishTheProviderRegistrationProcess()
        {
            _providerPage.verifyUserRegistration();
            _providerPage.continuLoginToNDIS();
            _loginPage.loginToApplication();
        }
        
        [Then(@"I Could able to finish the provider registration process and Login")]
        public void ThenICouldAbleToFinishTheProviderRegistrationProcessAndLogin()
        {
            _providerPage.verifyUserRegistration();
            _providerPage.continuLoginToNDIS();
            _loginPage.loginToApplication();
        }

        [When(@"I Click on Provider Portal")]
        public void WhenIClickOnProviderPortal()
        {
            _providerPage.ProviderSearch.Click();
        }

        [When(@"I Search for Random user and Click Register")]
        public void WhenISearchForRandomUserAndClickRegister()
        {
            _providerPage.SearchOrganizationByRandomName();
        }

        [When(@"I register by giving TradingName And All")]
        public void WhenIRegisterByGivingTradingNameAndAll()
        {
            _providerPage.AddYourOrganization();
        }

        [When(@"I select view location from user profile tab")]
        public void WhenISelectViewLocationFromUserProfileTab()
        {
            _providerPage.selectViewFromMyProfile();
        }
        [When(@"I Navigate to services")]
        public void WhenINavigateToServices()
        {
            Thread.Sleep(10000);
            _providerPage.NavigateToMyProfileNavLinks("Services");
            WebDriver.FindElement(By.Id("btnAddService")).Click();
        }

        [When(@"I get my security Code")]
        public async Task WhenIGetMySecurityCodeAsync()
        {

           String sCode = await _providerPage.getSecurityCodeAsync();

        }

        [When(@"I add my branding information")]
        public void WhenIAddMyBrandingInformation()
        {
            _providerPage.addBranding();
        }

        [When(@"I add my locations and Fecilities")]
        public void WhenIAddMyLocationsAndFecilities()
        {
            _providerPage.addLocations();
        }

        [Then(@"I should be navigated to Add Services page")]
        public void ThenIShouldBeNavigatedToAddServicesPage()
        {
            //ScenarioContext.Current.Pending();
        }
        //********************8888888
        [Then(@"I could see my profile page")]
        public void ThenICouldSeeMyProfilePage()
        {
            if (_providerPage.confirmProfilePage())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "User Can See My Profile Page", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "User Can not See My Profile Page ", null);
            }
        }

        [When(@"I navigate to Location Details page")]
        public void WhenINavigateToLocationDetailsPage()
        {
            _providerPage.selectViewFromMyProfile();
        }

        [When(@"I Click on Edit LOcations")]
        public void WhenIClickOnEditLOcations()
        {
            _providerPage.clickEditLocations();
        }

        [Then(@"I could see Add Locations page")]
        public void ThenICouldSeeAddLocationsPage()
        {
            if (_providerPage.confirmLocationDetailsPage())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "User Can See Add Location Details Page", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "User Can See Add Location Details Page ", null);
            }
        }
        [When(@"I Edit my locations and Fecilities")]
        public void WhenIEditMyLocationsAndFecilities()
        {
            _providerPage.EditLocations();
        }

        [Then(@"I should be able to Edit and Add services")]
        public void ThenIShouldBeAbleToEditAndAddServices()
        {
            _providerPage.addServices_edit();
        }

        //**************************
        [When(@"I Navigate to Locations")]
        public void WhenINavigateToLocations()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"I Could be able to Add Locations and fecilities")]
        public void ThenICouldBeAbleToAddLocationsAndFecilities()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"I should be able to add services")]
        public void ThenIShouldBeAbleToAddServices()
        {
            //Assert.IsTrue(_providerPage.AddServiceButton.Displayed);
            if (_providerPage.AddServiceButton.Displayed)
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "User Can See Add Services Option", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "User Can not See Add Services Option ", null);
            }

        }

        [When(@"I add Services")]
        public void WhenIAddServices()
        {
            _providerPage.addServices();
        }
        [When(@"I add Services edit")]
        public void WhenIAddServicesEdit()
        {
            _providerPage.addServices_edit();
        }


        [Then(@"I could able to add AssitanceWithDailyLiving options")]
        public void ThenICouldAbleToAddAssitanceWithDailyLivingOptions()
        {
           // Assert.IsTrue(_providerPage.SubmitButton.Displayed);
            if (_providerPage.SubmitButton.Displayed)
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "User Could AbleToAddAssitanceWithDailyLivingOptions", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "User failed ToAddAssitanceWithDailyLivingOptions ", null);
            }
        }

        [When(@"I add AssitanceWithDailyLiving options")]
        public void WhenIAddAssitanceWithDailyLivingOptions()
        {
            _providerPage.selectAssitanceWithDailyLiving();
        }

        [Then(@"I should be able to add workers")]
        public void ThenIShouldBeAbleToAddWorkers()
        {
           // Assert.IsTrue(_providerPage.AddStaffButton.Displayed);
            if (_providerPage.AddStaffButton.Displayed)
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "User Can See Add Staff Option", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "Add Staff option did not displayed ", null);
            }
        }

        [When(@"I select do it later")]
        public void WhenISelectDoItLater()
        {
            _providerPage.DoThisLaterButton.Click();
        }

        [Then(@"I should be able to see all my provider details")]
        public void ThenIShouldBeAbleToSeeAllMyProviderDetails()
        {
            //Assert.IsTrue(_providerPage.confirmOrganizationName());
            if (_providerPage.confirmOrganizationName())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "User confirmed OrganizationName", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "Failed to confirm OrganizationName", null);
            }
        }
        [Then(@"I should be able finish Organization profile")]
        public void ThenIShouldBeAbleFinishOrganizationProfile()
        {
            //Assert.IsTrue(_providerPage.confirmProfileFinished());
            if (_providerPage.confirmProfileFinished())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "User Completed Profile Set Up", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "Failed to Finsish Profile Setup process", null);
            }
        }


        [When(@"I Add Workers")]
        public void WhenIAddWorkers()
        {
            _providerPage.addWorkers();
        }

        [When(@"I search and add my Organization")]
        public void WhenISearchAndAddMyOrganization()
        {
            _providerPage.SearchOrganizationByName();
            _providerPage.AddYourOrganization();
        }

        [When(@"I search For my Organization")]
        public void WhenISearchForMyOrganization()
        {
            _providerPage.SearchOrganizationByName();
           
        }

        [When(@"I Cliam my organization")]
        public void WhenICliamMyOrganization()
        {
            _providerPage.claimTheOrganization();
        }
        [When(@"I go with security Code")]
        public void WhenIGoWithSecurityCode()
        {
            _providerPage.claimTheOrganizationWithSecurityCode();
        }

    }
}
