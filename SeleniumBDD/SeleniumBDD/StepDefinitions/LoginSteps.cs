﻿using OpenQA.Selenium;
using SeleniumBDD.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace SeleniumBDD.PageObjects
{
    [Binding]
    public class Login
    {
        public static IWebDriver WebDriver = TestSetup.WebDriver;
        public LoginPage myLoginPage;
        public Login()
        {
            myLoginPage = new LoginPage(WebDriver);
        }
        [Given(@"I Launch NDIS home Page")]
        public void GivenILaunchNDISHomePage()
        {
            myLoginPage.launchApplication();
        }
        [When(@"I Login with registered provider '(.*)' '(.*)'")]
        public void WhenILoginWithRegisteredProvider(string p0, string p1)
        {
            myLoginPage.loginToApplication(p0, p1);
        }


        [Given(@"I Called the Sign in Page")]
        public void GivenICalledTheSignInPage()
        {
            myLoginPage.callSignPage();
        }
        [When(@"I Click on SignIn")]
        public void WhenIClickOnSignIn()
        {
            myLoginPage.callSignPage();
        }

        [When(@"I Enter User Credentials and Submit")]
        public void WhenIEnterUserCredentialsAndSubmit()
        {
            myLoginPage.loginToApplication();
        }

        [Then(@"User login should be successful")]
        public void ThenUserLoginShouldBeSuccessful()
        {
            //ScenarioContext.Current.Pending();
        }

        [Then(@"I Logout from NDIS")]
        public void ThenILogoutFromNDIS()
        {
            myLoginPage.logoutApplication();
        }


    }
}
