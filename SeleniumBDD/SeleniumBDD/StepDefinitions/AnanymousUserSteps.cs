﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using SeleniumBDD.PageObjects;
using SeleniumBDD.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace SeleniumBDD.StepDefinitions
{
    [Binding]
    public class AnanymousUserSteps
    {
        public static IWebDriver WebDriver;
        public LoginPage _loginPage;
        public AnanymousUserPage _ananymous;
        public AnanymousUserSteps()
        {
            WebDriver = TestSetup.WebDriver;
            _loginPage = new LoginPage(WebDriver);
            _ananymous = new AnanymousUserPage(WebDriver);

        }
        [Given(@"As an Ananymous User I am on the NDIS home page")]
        public void GivenAsAnAnanymousUserIAmOnTheNDISHomePage()
        {
            _loginPage.launchApplication();
            _ananymous.ConfirmNDIShomePage();
        }

        [When(@"I Call Register on the Home Page")]
        public void WhenICallRegisterOnTheHomePage()
        {
            _ananymous.CallRegister();
        }

        [Then(@"I could see the user registration page")]
        public void ThenICouldSeeTheUserRegistrationPage()
        {
           Assert.IsTrue(_ananymous.confirmRegistrationForm());
        }

        [When(@"I submit the user details from")]
        public void WhenISubmitTheUserDetailsFrom()
        {
            _ananymous.fillRegistrationForm();
        }

        [Then(@"I could  see Thank you message")]
        public void ThenICouldSeeThankYouMessage()
        {
            Assert.IsTrue(_ananymous.ConfirmThankYouMessage());
        }

    }
}
