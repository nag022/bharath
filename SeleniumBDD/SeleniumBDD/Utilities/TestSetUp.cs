﻿using AventStack.ExtentReports.Reporter;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using As = AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using SeleniumBDD.PageObjects;

namespace SeleniumBDD.Utilities
{
    [Binding]
    public class TestSetup
    {
        public static IWebDriver WebDriver;
        public static String filepath;
        public static As.ExtentReports extentReports = new As.ExtentReports();
        public static As.ExtentTest NDISTest;
        public static As.ExtentTest currentTest;
        static String env = ConfigurationManager.AppSettings["EnvironmentName"];
        public static String Browser = ConfigurationManager.AppSettings["Browser"];
        static String browserVersion = ConfigurationManager.AppSettings["BrowserVersion"];
        static String os = ConfigurationManager.AppSettings["OS"];
        public static int TestCaseNumber = 0;
        public GmailPage _gmailPage = new GmailPage(WebDriver);
        [BeforeTestRun]
            public static void BeforeTestRun()
            {
            
            filepath = AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\Debug", "\\TestReports");
            if(! Directory.Exists(filepath))
              Directory.CreateDirectory(filepath);
            filepath = Path.Combine(filepath, "NDISTestReport" + new Random().Next(000, 999) + ".html");

            FileStream fs = File.Create(filepath);
            fs.Close();
            var htmlReporter = new ExtentHtmlReporter(filepath);
            extentReports.AttachReporter(htmlReporter);
            NDISTest = extentReports.CreateTest("NDIS Functional Tests", "               Environmet Details               ");
            NDISTest.Info("         Business Unit Name : NDIS       ", null);
            NDISTest.Info(" Environment :" + env + " ,Browser :" + Browser + " ,Browser Version :" + browserVersion + " ,Operating System :" + os, null);
            //KillChromeDriver();
            WebDriver = bringMyDriver();
            // WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(30));
            }

        public static IWebDriver bringMyDriver()
        {
            switch (ConfigurationManager.AppSettings["ExecutionType"])
            {
                case "Local":
                    WebDriver = GetLocalDriver(Browser);
                    break;
                case "Remote":
                    WebDriver = GetRemoteDriver(Browser);
                    break;
                default:
                    WebDriver = GetLocalDriver(Browser);
                    break;
            }
            return WebDriver;
        }

        [BeforeScenario]
            public void BeforeScenario()
            {

            if (TestCaseNumber > 0)
            {
                WebDriver = bringMyDriver();
               
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(30));
            }
            currentTest = NDISTest.CreateNode("TestName : " + ScenarioContext.Current.ScenarioInfo.Title);
            String ScenarioName = ScenarioContext.Current.ScenarioInfo.Title;
            // WebDriver.Manage().Timeouts().SetScriptTimeout(TimeSpan.FromMinutes(1));
            // WebDriver.Manage().Window.Maximize();
            if(ScenarioName.Equals("Register Provider"))
            {
                _gmailPage.launchGmail();
                _gmailPage.GmailLogin(0);
                _gmailPage.DeleteRegistrationEmail();
                _gmailPage.gmailLogout();
            }
           
             }

            [AfterScenario]
            public void AfterScenario()
            {
            TestCaseNumber++;
            string errorStep = null;
                var error = ScenarioContext.Current.TestError;
                if (error != null)
                {
                    var stackTrace = error.StackTrace;
                    errorStep = GetFailedStep(stackTrace);
                    currentTest.Log(AventStack.ExtentReports.Status.Pass, "Exception :" + errorStep, null);
                    return;
                }
                // WebDriver.Quit();
            }
            public string GetFailedStep(string stackTrace)
            {
                int first = stackTrace.IndexOf("FOEdge.Automation.GUI.SWYS.Tests.StepDefinitions.") + "FOEdge.Automation.GUI.SWYS.Tests.StepDefinitions.".Length;
                int last = stackTrace.IndexOf("() in", first);
                string str = stackTrace.Substring(first, last - first);
                return str;
            }

            [AfterTestRun]
            public static void AfterTestRun()
            {
                Thread.Sleep(6000);
                NDISTest.Info("        Total Tests Executed : " + TestCaseNumber, null);
                extentReports.Flush();
               // WebDriver.Quit();
            }
            private static void KillChromeDriver()
            {
                var processes = Process.GetProcessesByName("chromedriver");
                foreach (var currentProcess in processes)
                {
                    currentProcess.Kill();
                }
            }
            private static IWebDriver GetLocalDriver(string browser)
            {
                var options = new ChromeOptions();
                options.AddArguments("--test-type");
                return new ChromeDriver();
            }
            private static IWebDriver GetRemoteDriver(string browser)
            {
                var capabilities = new DesiredCapabilities();
                capabilities.SetCapability("name", ConfigurationManager.AppSettings["ApplicationName"]);
                capabilities.SetCapability(CapabilityType.BrowserName, browser);
                capabilities.SetCapability(CapabilityType.Version, ConfigurationManager.AppSettings["BrowserVersion"]);
                capabilities.SetCapability(CapabilityType.Platform, ConfigurationManager.AppSettings["OS"]);
                capabilities.SetCapability("screen-resolution", ConfigurationManager.AppSettings["ScreenResolution"]);
                capabilities.SetCapability("username", ConfigurationManager.AppSettings["SaucelabsUserName"]);
                capabilities.SetCapability("accessKey", ConfigurationManager.AppSettings["SaucelabsAccessKey"]);
                // capabilities.SetCapability(CapabilityType.IsJavaScriptEnabled, true);
                return new RemoteWebDriver(new Uri(ConfigurationManager.AppSettings["SaucelabsURL"]), capabilities);
            }
        }
    }

