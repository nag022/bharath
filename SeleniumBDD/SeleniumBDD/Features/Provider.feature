﻿Feature: Provider	

@NDIS-Provider
Scenario: Register Provider
	Given As Authorised provider I call NDIS home page
	When I Search for an organization by name
	And the searched organization is not listed in the NDIS database
	Then I should be suggested to Register a new organization
	When I clicks on Register a new organization button 
	Then I Should see the Registration Page
	When I fill all the Details and Submit the registration page
	Then I should be registered successfully
	When I Login To Gmail and Select BackToMyChooser
	Then I Could able to finish the provider registration process and Login
	When I Click on Provider Portal
	When I get my security Code
	When I Search for Random user and Click Register
	And I register by giving TradingName And All
	#And I Cliam my organization
	When I get my security Code
	When I search For my Organization
	And I go with security Code
	When I add my branding information
	When I add my locations and Fecilities
	Then I should be able to add services
	When I add Services
	Then I could able to add AssitanceWithDailyLiving options
	When I add AssitanceWithDailyLiving options
	Then I should be able to add workers
	When I select do it later
	Then I should be able to see all my provider details

	@NDIS-Provider
	Scenario: Edit Locations And Services to a Registered Provider with workers
	Given As Authorised provider I call NDIS home page
	When I Click on SignIn
	And I Enter User Credentials and Submit
	Then I could see my profile page
	When I navigate to Location Details page
	And I Click on Edit LOcations
	Then I could see Add Locations page
	When I Edit my locations and Fecilities
	Then I should be able to Edit and Add services

@NDIS-Provider
Scenario: Register Organization Without Workers
	Given As Authorised provider I call NDIS home page
	When I Search for an organization by name
	And the searched organization is not listed in the NDIS database
	Then I should be suggested to Register a new organization
	When I clicks on Register a new organization button 
	Then I Should see the Registration Page
	When I fill all the Details and Submit the registration page
	Then I should be registered successfully
	When I search For my Organization
	And I Cliam my organization
	When I get my security Code
	When I search For my Organization
	And I go with security Code
	When I add my branding information
	When I add my locations and Fecilities
	Then I should be able to add services
	When I add Services
	Then I could able to add AssitanceWithDailyLiving options
	When I add AssitanceWithDailyLiving options
	Then I should be able to add workers
	When I select do it later
	Then I should be able to see all my provider details

@NDIS-Provider
Scenario: Register Organization With Workers
	Given As Authorised provider I call NDIS home page
	When I Search for an organization by name
	And the searched organization is not listed in the NDIS database
	Then I should be suggested to Register a new organization
	When I clicks on Register a new organization button 
	Then I Should see the Registration Page
	When I fill all the Details and Submit the registration page
	Then I should be registered successfully
	When I search For my Organization
	And I Cliam my organization
	When I get my security Code
	When I search For my Organization
	And I go with security Code
	When I add my branding information
	When I add my locations and Fecilities
	Then I should be able to add services
	When I add Services
	Then I could able to add AssitanceWithDailyLiving options
	When I add AssitanceWithDailyLiving options
	Then I should be able to add workers
	When I Add Workers
	Then I should be able finish Organization profile
